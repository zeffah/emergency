package com.example.emegencyresponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class SubmitCase extends ActionBarActivity {
SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
	private final long ONE_DAY = 24 * 60 * 60 * 1000;
	
	final int trialPeriod = 0;
	
	String [] cases = {"Accident","Fire","Theft/Robbery","Escort","Other"};
	ListView listview;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_submit_case);
		
		checkExpiry();
		
		listview = (ListView)findViewById(R.id.listView1);	
		ListAdapter adapter = new ArrayAdapter<String>(this, R.layout.list_item, cases);
		listview.setAdapter(adapter);
		
		listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View v, int position,
					long id) {
				String selectedCase = ((TextView) v).getText().toString();
				
				ParseObject cases = new ParseObject("ReportedCases");
				cases.put("SubmittedCase", selectedCase);
				cases.put("user", ParseObject.createWithoutData ("_User", ParseUser.getCurrentUser().getObjectId()));
				cases.saveInBackground();
				
				//on successful submit
				startActivity(new Intent(SubmitCase.this, MapActivity.class));
			}
		});
	}
	private void checkExpiry(){
		SharedPreferences preferences = getPreferences(MODE_PRIVATE);
	    String installDate = preferences.getString("InstallDate", null);
	    if(installDate == null) {
	        // First run, so save the current date
	        SharedPreferences.Editor editor = preferences.edit();
	        Date now = new Date();
	        String dateString = formatter.format(now);
	        editor.putString("InstallDate", dateString);
	        // Commit the edits!
	        editor.commit();
	    }
	    else {
	        // This is not the 1st run, check install date
	    	try{
	    		
	        Date before = (Date)formatter.parse(installDate);
	        Date now = new Date();
	        long diff = now.getTime() - before.getTime();
	        long days = diff / ONE_DAY;
	        long daysleft = 30;
	        if(days >= trialPeriod) { // More than 30 days?
	        	
	        	checkForSubscription();
	        }else
	        	daysleft = trialPeriod-days;
	        	if(daysleft > 1){
	        		Toast.makeText(getApplicationContext(), "You have "+daysleft +"days left to your trial expiry", Toast.LENGTH_SHORT).show();
	        	}	        	
	    	}catch(ParseException e){
	        	e.printStackTrace();	        	
	        }
	    }
}
	private void checkForSubscription(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Subscriptions");
		query.whereEqualTo("UserID", ParseUser.getCurrentUser().getObjectId());
		
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> subscribed,
					com.parse.ParseException e) {
				if(e == null){
					for(ParseObject obj:subscribed){
						Boolean isSubscribed = obj.getBoolean("subscribed");
						if(isSubscribed == false){
							//expiry alert dialog
//							Toast.makeText(getApplicationContext(), "not successful", Toast.LENGTH_SHORT).show();
							 // Expired !!!
				        	Toast.makeText(getApplicationContext(), "Expired", Toast.LENGTH_SHORT).show();
				        	//LayoutInflater layoutInflater = LayoutInflater.from(SubmitCase.this);
							//View promptView = layoutInflater.inflate(R.layout.activity_rate_service, null);
							//ratingBar = (RatingBar)promptView.findViewById(R.id.ratingBar1);
							final AlertDialog alert = new AlertDialog.Builder(SubmitCase.this)
					       // .setView(promptView)
							.setMessage("The app is expired. Click 'subscribe' to enter mpesa transaction ID or hit 'how to subscibe' to see instructions on how to pay")
					        .setTitle("Oops!!")
					        .setPositiveButton("Subcribe", null) //Set to null. We override the onclick
					        .setNegativeButton("Lipa na Mpesa", null)
					        .setCancelable(false)
					        .create();
							
							alert.setOnShowListener(new DialogInterface.OnShowListener() {
								
								@Override
								public void onShow(DialogInterface dialog) {
									   Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
									   Button neg = alert.getButton(AlertDialog.BUTTON_NEGATIVE);
									   b.setOnClickListener(new View.OnClickListener() {
										
										@Override
										public void onClick(View view) {
											startActivity(new Intent(SubmitCase.this, Subscribe.class));
										}
									});
									   
									   neg.setOnClickListener(new View.OnClickListener() {
											
											@Override
											public void onClick(View view) {
												Intent intent = new Intent(Intent.ACTION_MAIN);
											    intent.setComponent(ComponentName.unflattenFromString("com.android.stk"));
											    intent.addCategory(Intent.CATEGORY_LAUNCHER);
											    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
											    startActivity(intent);
											}
										});
								}
							});
							
							alert.show();
							alert.setCanceledOnTouchOutside(false);
							
							//end of alertDialog
						}else
							Toast.makeText(getApplicationContext(), "successful", Toast.LENGTH_SHORT).show();
					}
				}
			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.submit_case, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.logout) {
			ParseUser.logOut();
			startActivity(new Intent(SubmitCase.this, Login.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
