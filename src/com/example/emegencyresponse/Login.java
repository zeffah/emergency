package com.example.emegencyresponse;

import java.util.List;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;


public class Login extends ActionBarActivity implements LocationListener {
	double lat, lon;
	String username, password;
	EditText uName, pWord;
	Button login;
	ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActionBar ab = getActionBar();
        ab.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffffff")));
        
        if(ParseUser.getCurrentUser() != null){
            this.finish();
            startActivity(new Intent(Login.this, SubmitCase.class));
            }
        
        uName = (EditText)findViewById(R.id.et_username);
        pWord = (EditText)findViewById(R.id.et_password);
        login = (Button)findViewById(R.id.btn_login);
        
        login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				pDialog = new ProgressDialog(Login.this);
				pDialog.setMessage("Please wait...");
				pDialog.setIndeterminate(false);
				pDialog.setCancelable(false);
				pDialog.show();
				username = uName.getText().toString();
				password = pWord.getText().toString();
				
				ParseUser.logInInBackground(username, password, new LogInCallback(){
					@Override
					public void done(ParseUser user, ParseException e) {
						pDialog.dismiss();
						if(user != null){
							startActivity(new Intent(Login.this, SubmitCase.class));
							checkLocDetails();
						}else{
							Toast.makeText(
									getApplicationContext(),
									"No such user exist, please signup",
									Toast.LENGTH_LONG).show();
						}
					}
				});
			}
		});
        ((TextView)findViewById(R.id.signup)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				startActivity(new Intent(Login.this, BasicDetails.class));
				finish();
			}
		});
    }

	@Override
	public void onLocationChanged(Location location) {
//		double latitude = location.getLatitude();
//        double longitude = location.getLongitude();
//        LatLng latLng = new LatLng(latitude, longitude);
	}

	@Override
	public void onProviderDisabled(String arg0) {
	}

	@Override
	public void onProviderEnabled(String arg0) {
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}
	private void checkLocDetails(){
//        check if location data alredy sent..if n
        ParseQuery<ParseObject> query = ParseQuery.getQuery("Location");
		query.whereMatches("loggedUser", ParseUser.getCurrentUser().get("username").toString());
		
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> dataSent, com.parse.ParseException e) {
				if(e == null){
//					Toast.makeText(getApplicationContext(), "data already sent", Toast.LENGTH_SHORT).show();
					for(ParseObject obj:dataSent){
////						Boolean isSubscribed = obj.getBoolean("subscribed");
						String name = obj.getString("username");
						obj.put("loggedUser", "aaaaaaa");
//						if(!name.equals("")){
//							//expiry alert dialog
////							Toast.makeText(getApplicationContext(), "data already sent", Toast.LENGTH_SHORT).show();
//							
//						}else{
//					        //send the users location to db
//					        ParseObject loc = new ParseObject("Location");
//						    loc.put("loggedUser", ParseUser.getCurrentUser().get("username"));
//						    loc.put("loginLat", lat);
//						    loc.put("loginLon", lon);
//						    loc.saveInBackground();
//							//Toast.makeText(getApplicationContext(), "successful", Toast.LENGTH_SHORT).show();
//							}
					}
				}else{
					LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
			        Criteria criteria = new Criteria();
			        String bestProvider = locationManager.getBestProvider(criteria, true);
			        Location location = locationManager.getLastKnownLocation(bestProvider);
			        lat = location.getLatitude();
			        lon = location.getLongitude();    
					//send the users location to db
			        ParseObject loc = new ParseObject("Location");
				    loc.put("loggedUser", ParseUser.getCurrentUser().get("username"));
				    loc.put("loginLat", lat);
				    loc.put("loginLon", lon);
				    loc.saveInBackground();
				    
				    
			        if (location != null) {
			            onLocationChanged(location);
			        }
			        locationManager.requestLocationUpdates(bestProvider, 20000, 0, Login.this);
					finish();
				}
			}
		});
	}
}
