package com.example.emegencyresponse;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class Subscribe extends Activity {
	String txnID, phone;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_subscribe);
		 	
		((Button)findViewById(R.id.btn_subscribe)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				txnID = ((TextView)findViewById(R.id.ettxn_id)).getText().toString();
				phone = ((TextView)findViewById(R.id.etsubphone)).getText().toString();

				ParseObject subs = new ParseObject("Subscriptions");
				subs.put("TransactionID", txnID);
				subs.put("phoneNumber", phone);
				subs.put("UserID", ParseObject.createWithoutData ("_User", ParseUser.getCurrentUser().getObjectId()));
				subs.saveInBackground();
				
				checkTXNID();
			}
		});		
	}
	private void checkTXNID(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Subscriptions");
		query.whereEqualTo("mpesaTxnID", txnID);
		
		query.findInBackground(new FindCallback<ParseObject>(){

			@Override
			public void done(List<ParseObject> txn_id, ParseException e) {
				if(e == null){
					for(ParseObject obj:txn_id){
						String id = obj.getString("mpesaTxnID");
					Toast.makeText(getApplicationContext(), ""+id, Toast.LENGTH_LONG).show();

					startActivity(new Intent(Subscribe.this, SubmitCase.class));
					}
				}else
					Toast.makeText(getApplicationContext(), "ID not exit", Toast.LENGTH_SHORT).show();
			}
			
		});
	}
}
