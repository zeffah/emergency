package com.example.emegencyresponse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

public class SignUp extends ActionBarActivity {
	String  username, password, confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);
		
		((Button) findViewById(R.id.btn_signup)).setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				username = ((EditText)findViewById(R.id.etusername)).getText().toString();
				password =  ((EditText)findViewById(R.id.etpassword)).getText().toString();
				confirm =  ((EditText)findViewById(R.id.etre_enterpassword)).getText().toString();
				
				if(username.isEmpty() && password.isEmpty()){
					Toast.makeText(getApplicationContext(),
							"Please complete the sign up form",
							Toast.LENGTH_LONG).show();
				}else{
					// Save new user data into Parse.com Data Storage
					ParseUser user = new ParseUser();
					user.setUsername(username);
					user.setPassword(password);
			
					user.signUpInBackground(new SignUpCallback(){

						@Override
						public void done(ParseException e) {
							if(e == null){
								startActivity(new Intent(SignUp.this, Login.class));
							}else{
								Toast.makeText(getApplicationContext(),
										"Sign up Error", Toast.LENGTH_LONG)
										.show();
							}
						}	
					});
				}
			}
		});
	}
}
