package com.example.emegencyresponse;

import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.provider.SyncStateContract.Constants;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

public class MapActivity extends FragmentActivity  implements LocationListener{
	//private static final LatLng Event_place = new LatLng(-1.29207, 36.82195);
	 Criteria criteria = new Criteria();
	 LocationManager locationManager;
	double lat, lon;
    private GoogleMap map;
    Button rate, noHelp;
    RatingBar ratingBar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//show error dialog if GoolglePlayServices not available
        if (!isGooglePlayServicesAvailable()) {
            finish();
        }
		setContentView(R.layout.activity_map);
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
	            .getMap();
		map.setMyLocationEnabled(true);
		 map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
		 getLocation();
	       // LatLng userLocation = new LatLng(lat, lon);
	    //map.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 15));
		 getCordinates();
	   rate = (Button)findViewById(R.id.btn_rate);
	     noHelp =  (Button)findViewById(R.id.btn_no_help);
	     rate.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View view) {
				LayoutInflater layoutInflater = LayoutInflater.from(MapActivity.this);
				View promptView = layoutInflater.inflate(R.layout.activity_rate_service, null);
				ratingBar = (RatingBar)promptView.findViewById(R.id.ratingBar1);
				
				final AlertDialog alert = new AlertDialog.Builder(MapActivity.this)
		        .setView(promptView)
		        .setTitle("Rate the services")
		        .setPositiveButton("Rate", null) //Set to null. We override the onclick
//		        .setNegativeButton(android.R.string.cancel, null)
		        .create();
				
				alert.setOnShowListener(new DialogInterface.OnShowListener() {
					
					@Override
					public void onShow(DialogInterface dialog) {
						   Button b = alert.getButton(AlertDialog.BUTTON_POSITIVE);
						   b.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View view) {
								float userRating = ratingBar.getRating();
								ParseObject rates = new ParseObject("Rates");
								rates.put("rating_value", userRating);
								rates.put("user", ParseObject.createWithoutData ("_User", ParseUser.getCurrentUser().getObjectId()));
								alert.dismiss();
								Log.d("currentUser", String.valueOf(userRating));
							}
						});
					}
				});
				
				alert.show();
				alert.setCanceledOnTouchOutside(false);
			}
		});
	    
	}
	
//	private void setOnRatingBar(){
//
//	}
protected void getLocation(){
	if (isLocationEnabled(MapActivity.this)) {
		locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		 criteria = new Criteria();
		 String bestProvider = locationManager.getBestProvider(criteria, true);
		 bestProvider = String.valueOf(locationManager.getBestProvider(criteria, true)).toString();
		 //You can still do this if you like, you might get lucky:
         Location location = locationManager.getLastKnownLocation(bestProvider);

         if (location != null) {
             Log.e("TAG", "GPS is on");
             lat = location.getLatitude();
             lon = location.getLongitude();
             
             LatLng userLocation = new LatLng(lat, lon);
 	        Marker myLoc = map.addMarker(new MarkerOptions()
 		    .position(userLocation)
 		    .title("My location")
 		    .snippet("Currentplace !!!")
 		    );
 	       map.moveCamera(CameraUpdateFactory.newLatLngZoom(userLocation, 20));
             Toast.makeText(MapActivity.this, "latitude:" + lat + " longitude:" + lon, Toast.LENGTH_SHORT).show();
             if (location != null) {
 	            onLocationChanged(location);
 	        }
         }
         else{
             //This is what you need:
             locationManager.requestLocationUpdates(bestProvider, 20000, 0, MapActivity.this);
         }
	}else{
		//prompt user to enable location services
		 AlertDialog.Builder dialog = new AlertDialog.Builder(MapActivity.this);
	        dialog.setMessage("GPS not enabled");
	        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

	            @Override
	            public void onClick(DialogInterface dialog, int which) {
	                //this will navigate user to the device location settings screen
	                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
	                startActivity(intent);
	            }
	        });
	        AlertDialog alert = dialog.create();
	        alert.show();
	}
}
public boolean isLocationEnabled(Context mContext) 
{
    LocationManager lm = (LocationManager)
            mContext.getSystemService(Context.LOCATION_SERVICE);
    return lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
   }
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, this, 0).show();
            return false;
        }
    }

	@Override
	public void onLocationChanged(Location location) {
		  double latitude = location.getLatitude();
	        double longitude = location.getLongitude();
	        LatLng latLng = new LatLng(latitude, longitude);
	        map.addMarker(new MarkerOptions().position(latLng));
	        map.moveCamera(CameraUpdateFactory.newLatLng(latLng));
	        map.animateCamera(CameraUpdateFactory.zoomTo(15));
	}

	@Override
	public void onProviderDisabled(String arg0) {
	}

	@Override
	public void onProviderEnabled(String arg0) {
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
	}
	
	@SuppressWarnings("deprecation")
	private void getCordinates(){
		Date midnight = new Date();
		midnight.setHours(0);
		midnight.setMinutes(0);
		midnight.setSeconds(0);
		
		Date endDay = new Date();
		endDay.setHours(23);
		endDay.setMinutes(59);
		endDay.setSeconds(59);
		 ParseQuery<ParseObject> query = ParseQuery.getQuery("Location");
			//query.whereMatches("loggedUser", ParseUser.getCurrentUser().getUsername().toString());
//		 query.whereGreaterThan("createdAt", midnight);
//		 query.whereLessThan("createdAt", endDay);
			
			query.findInBackground(new FindCallback<ParseObject>(){

				@Override
				public void done(List<ParseObject> dataSent, com.parse.ParseException e) {
					if(e == null){
						Toast.makeText(MapActivity.this, "found", Toast.LENGTH_SHORT).show();
						for(ParseObject obj:dataSent){
							double  lat = obj.getDouble("loginLat");
							double  lon = obj.getDouble("loginLon");
							 LatLng latLng = new LatLng(lat, lon);
							 map.addMarker(new MarkerOptions().position(latLng));
						}
					}else{
						Toast.makeText(MapActivity.this, "not found", Toast.LENGTH_SHORT).show();
					}
				}
			});
	}
}
