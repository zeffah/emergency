package com.example.emegencyresponse;

import java.util.List;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class BasicDetails extends ActionBarActivity {
	ProgressDialog pd;
	String fname,lname, id, phone, location;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_basic_details);
		((Button)findViewById(R.id.btn_basic_details)).setOnClickListener(new View.OnClickListener() {		
			@Override
			public void onClick(View v) {
				
				 pd = new ProgressDialog(BasicDetails.this);
					pd.setMessage("saving...");
					pd.setIndeterminate(false);
					pd.setCancelable(false);
					pd.show();
					
				fname = ((EditText)findViewById(R.id.etfname)).getText().toString();
				lname = ((EditText)findViewById(R.id.etlname)).getText().toString();
				id = ((EditText)findViewById(R.id.etnat_id)).getText().toString();
				phone = ((EditText)findViewById(R.id.etphone)).getText().toString();
				location = ((EditText)findViewById(R.id.etlocation)).getText().toString();
				
				ParseObject myUser = new ParseObject("ClientUsers");
				myUser.put("firstname", fname);
				myUser.put("lastname", lname);
				myUser.put("nationalID", id);
				myUser.put("mobilePhone", phone);
				myUser.put("Location", location);
				myUser.saveInBackground();
				
				getCompanyId(phone);
			}
		});
	}
	
	public void getCompanyId(String phoneNumber){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("ClientUsers");
		query.whereEqualTo("mobilePhone", phoneNumber);
		
		query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> userID, ParseException e) {

                if (e == null) {
                	pd.dismiss();
                	for (ParseObject obj : userID){
                		String userIdentity = obj.getObjectId();
                		Intent i = new Intent(BasicDetails.this, SignUp.class);
                		i.putExtra("identity", userIdentity);
                		startActivity(i);
                		 Log.d("currentUser", ""+userIdentity);
                	}
//                    if (coID.size() > 0) {
//                    }
//                    else {
//                    }
                }
                else {
                    // Shit happened!
                    AlertDialog.Builder builder = new AlertDialog.Builder(BasicDetails.this);
                    builder.setMessage(e.getMessage())
                            .setTitle("Oops!")
                            .setPositiveButton(android.R.string.ok, null);
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
            }
        });
	}
}
